var milkcocoa = new MilkCocoa('vueioshedrc.mlkcca.com');

var ds = milkcocoa.dataStore("chat");
var textArea, board;
window.onload = function(){
  textArea = document.getElementById("msg");
  board = document.getElementById("board");
}

function clickEvent(){
  var text = textArea.value;
  sendText(text);
}

function sendText(text){
  ds.push({message : text},function(data){
    textArea.value = "";
  });
}

ds.on("push",function(data){
  addText(data.value.message);
});

function addText(text){
  var msgDom = document.createElement("li");
  msgDom.innerHTML = text;
  board.insertBefore(msgDom, board.firstChild);
}

ds.stream().size(30).next(function(err, data) {
  if(err) console.log(err);
  data.forEach(function(d) {
    console.log(d);
    addText(d.value.message);
  });
});
